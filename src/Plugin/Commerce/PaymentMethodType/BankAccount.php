<?php

namespace Drupal\commerce_sepa\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides a bank account payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "bank_account",
 *   label = @Translation("Bank account"),
 * )
 */
class BankAccount extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    $t_args = [
      '@account_number' => substr(str_replace(' ', '', $payment_method->iban->value), -4),
    ];

    return $this->t('Bank account ending in @account_number', $t_args);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['iban'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('IBAN'))
      ->setDescription($this->t('The International Bank Account Number.'))
      ->setSetting('max_length', 34)
      ->setRequired(TRUE);
    $fields['bic'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('BIC number'))
      ->setRequired(TRUE);
    $fields['account_holder'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Account holder'))
      ->setRequired(TRUE);

    return $fields;
  }

}
