<?php

namespace Drupal\commerce_sepa\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\Core\Form\FormStateInterface;

class SepaPaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['payment_details'] = $this->buildBankAccountForm($form['payment_details'], $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->validateBankAccountForm($form['payment_details'], $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->submitBankAccountForm($form['payment_details'], $form_state);

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Builds the bank account form.
   *
   * @param array $element
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The built bank account form.
   */
  protected function buildBankAccountForm(array $element, FormStateInterface $form_state) {
    $element['#attributes']['class'][] = 'bank-account-form';
    $element['iban'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IBAN'),
      '#attributes' => ['autocomplete' => 'off'],
      '#required' => TRUE,
      '#maxlength' => 34,
      '#size' => 34,
      // These keys are used only for theme suggestions.
      // @see system_theme_suggestions_field().
      '#field_name' => 'iban',
      '#field_type' => 'commerce_sepa',
    ];

    /** @var \Drupal\commerce_sepa\Plugin\Commerce\PaymentGateway\Sepa $plugin */
    $plugin = $this->plugin;
    $config = $plugin->getConfiguration();
    $element['bic'] = [
      '#type' => 'textfield',
      '#title' => $this->t('BIC number'),
      '#maxlength' => 11,
      '#size' => 11,
      '#access' => $config['bic'] ?? FALSE,
    ];
    $element['account_holder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account holder'),
      '#access' => $config['account_holder'] ?? FALSE,
    ];

    return $element;
  }

  /**
   * Validates the bank account form.
   *
   * @param array $element
   *   The bank account form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  protected function validateBankAccountForm(array &$element, FormStateInterface $form_state) {
    $gateway_settings = $this->plugin->getConfiguration();
    $values = $form_state->getValue($element['#parents']);

    // Verify the entered BIC number:
    if (!empty($values['bic'])) {
      if (!$this->verifyBic($values['bic'])) {
        $form_state->setError($element['bic'], $this->t('You have entered an invalid BIC number.'));
      }
    }

    // Convert IBAN to human format.
    $values['iban'] = iban_to_human_format($values['iban']);

    if (!verify_iban($values['iban']) ||
      (count($gateway_settings['valid_countries']) &&
        !in_array(iban_get_country_part($values['iban']), $gateway_settings['valid_countries']))) {
      $form_state->setError($element['iban'], $this->t('You have entered an invalid bank account number.'));
    }
    else {
      // Persist the modified values.
      $form_state->setValueForElement($element['iban'], $values['iban']);
    }
  }

  /**
   * Validates the given BIC number to verify it meets the ISO_9362.
   *
   * @param string $bic
   *   The BIC number to be verified.
   *
   * @return bool
   *   TRUE if the given BIC meets the rules specified by
   *   https://en.wikipedia.org/wiki/ISO_9362.
   */
  protected function verifyBic(string $bic) : bool {
    $bic_pattern = '/^([a-zA-Z]){4}([a-zA-Z]){2}([0-9a-zA-Z]){2}([0-9a-zA-Z]{3})?$/';
    return (bool) preg_match($bic_pattern, $bic);
  }

  /**
   * Handles the submission of the bank account form.
   *
   * @param array $element
   *   The bank account form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  protected function submitBankAccountForm(array $element, FormStateInterface $form_state) {
    $values = $form_state->getValue($element['#parents']);

    $this->entity->iban = $values['iban'];
    $this->entity->bic = $values['bic'] ?? '';
    $this->entity->account_holder = $values['account_holder'] ?? '';
  }

}
